local default_config = {
  menu_items = {}, -- override the default items:: use it when you don't want the plugin provided menu_items
  enable_log = true, -- enable error log be printed out. Turn it off if you don't want see those lines
}

vim.g.context_menu_config = default_config
